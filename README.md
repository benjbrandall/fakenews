1. Install Ruby (at least version 2.3) on your computer. Use rubyinstaller for Windows, homebrew for macOS, and ruby-dev for Linux.
2. Run this in the terminal: gem install bundler
3. Navigate to this README's directory in terminal, and type: bundle
4. If the bundle was successful, you can run the bot with: ruby bot.rb

The bot will read from the csv file in its directory named urls.csv
