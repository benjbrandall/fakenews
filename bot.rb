require 'twitter'
require 'csv'

class Worker

  def initialize(client, pair)
    pair = pair.first
    @client = client
    @fake_news_url = pair["fake_news_url"]
    @debunk_url = pair["debunk_url"]
    @answer_text = pair["answer_text"]
  end

  def client
    @client
  end

  def get_random_tweet(user)
    tweets = [
      "Olá @#{user}! O link que você compartilhou não parece ser de uma notícia verdadeira!",
      "Olá @#{user}! Essa informação não é totalmente verdadeira!",
      "Olá @#{user}! Essa notícia carece de fontes apropriadas!",
      "Olá @#{user}! O link que você compartilhou é parte de uma corrente!",
      "Olá @#{user}! O link que você compartilhou incita práticas criminosas!",
      "Olá @#{user}! O link que você compartilhou pertence a um site não confiável!",
      "Olá @#{user}! Essa notícia já foi apagada por ser falsa!",
      "Olá @#{user}! Os dados apresentados nesse link não são de uma pesquisa verificada!",
      "Olá @#{user}! As afirmações encontradas nesse link representam falas perigosas!",
      "Olá @#{user}! Conheça mais sobre o nosso projeto aqui!"
    ]
    return tweets.sample
  end

  def search_for_tweeted_link
    tweets = []
    results = @client.search(@fake_news_url, options = {:include_entities => true, :result_type => "recent"})
    results.attrs[:statuses].each do |status|
      tweets.push(status)
    end
    @tweets = tweets
  end

  def respond_to_matched_tweet_ids
    @tweets.each do |tweet| # CHANGE TO EACH
      user = tweet[:user][:screen_name]
      id = tweet[:id]
      p @client.update("Olá @#{user}! #{@answer_text} #{@debunk_url}", options = {:in_reply_to_status_id => tweet[:id]})
    end
  end

end

client = Twitter::REST::Client.new do |config|
  config.consumer_key = "" # !! add consumer key between the quotes
  config.consumer_secret = "" # !! add consumer secret key between the quotes
  config.access_token = "" # !! add access token between the quotes
  config.access_token_secret = "" # !! add access secret key between the quotes
end

#

url_pairs = []

CSV.foreach('urls.csv', headers: true) do |row|
  url_pairs.push([row.to_h])
end

url_pairs.each do |pair|
  worker = Worker.new(client, pair.flatten)
  worker.search_for_tweeted_link
  worker.respond_to_matched_tweet_ids
end
